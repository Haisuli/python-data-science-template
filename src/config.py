import os
from pathlib import Path

from dotenv import load_dotenv

load_dotenv()

# VARIABLES
# Create variables from environmental variables,
# possibly specified in the .env file
DATA_PATH = Path(os.getenv("DATA_PATH", "/"))
